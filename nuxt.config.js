import colors from "vuetify/es5/util/colors";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s - jobmai",
    title: "jobmai",
    htmlAttrs: {
      lang: "th",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~/assets/css/main.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    treeShake: true,
    font: {
      family: "Mitr",
    },
    theme: {
      dark: false,
      light: true,
      themes: {
        light: {
          "ci-green": "#3A8689",
          "ci-lgreen": "#D9E9EA",
          "ci-dgreen": "#13474E",
          "ci-purple": "#3E51B5",
          "ci-lpurple": "#F0F3FF",
          "ci-black": "#000000",
          "ci-white": "#FFFFFF",
          "ci-gray": "#C9C9C9",
          "ci-graylt1": "#EDEEED",
          "ci-graylt2": "#F7F7F7",
          "ci-graydk1": "#868686",
        },
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  axios: {
    baseURL: `${process.env.SERVER_URL}`,
  },
  server: {
    port: 3000,
    host: "0.0.0.0",
  },
};
